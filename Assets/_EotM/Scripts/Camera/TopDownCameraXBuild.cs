﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TopDownCameraXBuild: MonoBehaviour {

	public float dampTime = .2f;
	public float screenEdgeBuffer = 2f; // How much space will be left between the screen and player
	public float minSize = 6.5f; // Smallest size the camera can be
	public float maxSize = 15f; // Largest size the camera can be
  
	public List<GameObject> players = new List<GameObject>() ;
	private Camera camera;
	private float zoomSpeed;
	private Vector3 moveVelocity;
	private Vector3 desiredPosition;
    private bool SetUp = false;
	private void Awake() {
		camera = GetComponent<Camera> ();		
	}

	private void Start() {
		//SetupCamera();
	}

    public void AddFocusPoint(GameObject go){
        players.Add(go);
    }

	public void SetupCamera() {
		//players = GameObject.FindGameObjectsWithTag ("Player");	
		SetStartPositionAndSize ();
	}

	private void Update() {
        if (SetUp)
        {
            
            Move();

            Zoom();
        }
	}

	private void Move() {
		FindAveragePosition ();
	
		// Move the camera towards the desired position
		transform.position = Vector3.SmoothDamp (transform.position, desiredPosition, ref moveVelocity, dampTime);
	}

	private void FindAveragePosition() {
		Vector3 averagePos = Vector3.zero;

		// Find the sum of all the player positions
		for (int i = 0; i < players.Count; i++) {
			averagePos += players [i].transform.position;
		}

		// Calculate the average by dividing by the number of players
		averagePos /= players.Count;
      
		averagePos.z = transform.position.z; // Keep the same z value

		// Set the desired postion of the camera
		desiredPosition = averagePos;
	}

	private void Zoom() {
		float requiredSize = FindRequiredSize ();

		// Change the size of the camera towerds the desired size
		camera.orthographicSize = Mathf.SmoothDamp (camera.orthographicSize, requiredSize, ref zoomSpeed, dampTime);
	}

	private float FindRequiredSize() {
		Vector3 desiredLocalPos = transform.InverseTransformPoint(desiredPosition);
		float size = 0f;

		for (int i = 0; i < players.Count; i++) {

			//Find position of target in camera's local space
			Vector3 targetLocalPos = transform.InverseTransformPoint (players [i].transform.position);
		
			//Find position of the target from the desired position of the camera
			Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

			//Choose the largest out of the current size and distance of the player in the vertical direction
			size = Mathf.Max (size, Mathf.Abs(desiredPosToTarget.y));

			//Choose the largest out of the current size and distance of the player in the horizontal direction 
			size = Mathf.Max (size, Mathf.Abs (desiredPosToTarget.x) / camera.aspect);
		}

		// Add the buffer to the size
		size += screenEdgeBuffer;

		if (size > maxSize) {
			Debug.Log ("A player is moving outside of the possible movement area." +
				" We should stop his movemnt or adjust the max size for the camera");
		}

		// Make sure the size isn't below the minumum.
		size = Mathf.Clamp (size, minSize, maxSize);

		return size;
	}

	private void SetStartPositionAndSize() {
        SetUp = true;
		FindAveragePosition ();

		transform.position = desiredPosition;

		camera.orthographicSize = FindRequiredSize ();
	}
}