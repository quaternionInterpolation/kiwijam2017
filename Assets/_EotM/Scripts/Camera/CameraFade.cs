﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MovementEffects;

public class CameraFade : MonoBehaviour
{
    public Image ImageScreenFade;
   
    public float increment;

    public bool IN=false;
    public bool OUT=false;
	// Use this for initialization
	void Awake ()
    {
        //if (GameManager.Instance.Faded)
        //{
        //    ImageScreenFade.color = Color.black;
        //    StartCoroutine(FadeIn());
        //}
        //else
        //{
        //    Color temp = Color.black;
        //    temp.a = 0;
        //    ImageScreenFade.color = temp;
        //}

    }
    private void Update()
    {
        if (IN)
        {
            IN = false;
            fadeIn();
        }
        if (OUT)
        {
            OUT = false;
            fadeOut();

        }
    }
    public void fadeOut()
    {
        StartCoroutine(FadeOut());
    }
	public void fadeIn()
    {
        StartCoroutine(FadeIn());
    }
	private IEnumerator<float> FadeOut()
    {
        Color temp = ImageScreenFade.color;
        while (ImageScreenFade.color.a < 1)
        {
            temp.a += increment;
            ImageScreenFade.color = temp;
            yield return Timing.WaitForOneFrame;

        }
        temp.a = 1;
        ImageScreenFade.color = temp;

    }
    private IEnumerator<float> FadeIn()
    {
        Color temp = ImageScreenFade.color;
        while (ImageScreenFade.color.a > 0.001f)
        {
            temp.a -= increment;
            ImageScreenFade.color = temp;
            yield return Timing.WaitForOneFrame;

        }
        temp.a = 0;
        ImageScreenFade.color = temp;

    }

}
