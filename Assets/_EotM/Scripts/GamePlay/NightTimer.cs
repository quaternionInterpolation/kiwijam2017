﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class NightTimer : MonoBehaviour
{
    public float NightStartTimer;
    private float CurrentTimer;
    private BasicGameMode GameState;

    private void Start()
    {
        GameState = FindObjectOfType<BasicGameMode>();
    }
    // Update is called once per frame
    void Update()
    {
        if (GameState.curState == BasicGameMode.State.Setup)
        {
            CurrentTimer += Time.deltaTime;
            if (CurrentTimer > NightStartTimer)
            {
                try
                {
                    FindObjectOfType<Lighting>().StartNight();
                    PlayerData[] players = FindObjectOfType<GameMode>().GetPlayers().ToArray();
                    players[Random.Range(0, players.Length)].gamePlayer.SetBetrayer(true);
                }
                catch (System.Exception)
                {

                    throw;
                }
                Destroy(gameObject);
            }
        }
    }
}
