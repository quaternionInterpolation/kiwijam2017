﻿﻿using System.Collections;
using System.Collections.Generic;
using MovementEffects;
using Spine.Unity;
using UnityEngine;


public class EMPlayerXBuild : EMPlayerBase
{
    //   private static readonly string ANIM_ACCUSEWRONG = "butler_accuseWrong";
    //   private static readonly string ANIM_IDLE_BACK = "butler_back_idle";
    //   private static readonly string ANIM_IDLE_FRONT = "butler_front_idle";
    //   private static readonly string ANIM_IDLE_SIDE = "butler_side_idle";
    //   private static readonly string ANIM_TASK_BACK = "butler_back_task";
    //   private static readonly string ANIM_TASK_FRONT = "butler_front_task";
    //   private static readonly string ANIM_TASK_SIDE = "butler_side_task";
    //   private static readonly string ANIM_WALK_BACK = "butler_back_walk";
    //   private static readonly string ANIM_WALK_FRONT = "butler_front_walk";
    //private static readonly string ANIM_WALK_SIDE = "butler_side_walk";
    //private static readonly string ANIM_DIE = "butler_die";
    public enum PlayerState
    {
        ALIVE,
        FIRED,
        KILLED
    }

    public enum PlayerDirection
    {
        FRONT,
        BACK,
        SIDE
    }

    [Header ("Movement Variables")]
	[SerializeField]
	[Tooltip ("Total speed possible")]
	protected float Speed;
	[SerializeField]
	[Tooltip ("Sprint multiplyer")]
	protected float SprintModifier;
	[SerializeField]
	[Tooltip ("How fast player accelerates & changes direction")]
	protected float maxVelocity;
	//[SerializeField]
	//protected bool Sprinting = false;
   
	public GameObject TempGameModel;
	[Header ("Interactable Section")]
	public List<InteractableObject> InteractedList = new List<InteractableObject> ();

	public InteractableObject CurrentTriggerObject;
	public InteractableObject currentTrappedObject;
    //public bool interacting = false;
    public bool _interacting = false;
    public bool interacting {
        get { return _interacting; }
        set { _interacting = value; Anim.SetBool("performing_task", value);}
    }

	public Sprite[] buttonSprites;
	public UnityEngine.UI.Image buttonRenderer;
	public Canvas timerRenderer;
	public UnityEngine.UI.Image timerImage;
    public Transform animRoot;

	private const float PLAYER_LIFETIME_S = 15.0f;
	private float life = PLAYER_LIFETIME_S;

	private const float PLAYER_FAILURE_PENALTY_S = 0.0f;

    private PlayerDirection direction = PlayerDirection.FRONT;

    private PlayerData data;

    public bool isBetrayer = false;

	private PlayerState state = PlayerState.ALIVE;

	// [Header("Player Controller Variables")]

	private Rigidbody2D RB;
	//just incase
	private Animator Anim {
		get {
			return GetComponentInChildren<Animator> ();
		}
	}
	//adds trigger to list
	public void SetTrigger (InteractableObject _trigger)
	{
		CurrentTriggerObject = _trigger;

	}

    public override void SetPlayerData(PlayerData data)
    {
        this.data = data;
        //SetBetrayer(true);
    }

    public override void SetBetrayer(bool isBetrayer){
        this.isBetrayer = isBetrayer;

        if (isBetrayer){
            Timing.RunCoroutine(BetrayVibration());
        }
    }

    private IEnumerator<float> BetrayVibration(){
        Rewired.Joystick js = data.player.controllers.joystickCount > 0 ? data.player.controllers.Joysticks[0] : null;
        if (js != null){
            Debug.Log("Attempting vibration on controller: "+js.name+", can vibe: "+js.supportsVibration);
            data.player.controllers.Joysticks[0].SetVibration(0.5f, 0.5f);
			yield return Timing.WaitForSeconds(1.0f);
			data.player.controllers.Joysticks[0].SetVibration(0f, 0f);
        }
    }

	public void RemoveTrigger ()
	{
		CurrentTriggerObject = null;
	}

	// Use this for initialization
	void Start ()
	{
		RB = GetComponent<Rigidbody2D> ();
		timerRenderer.transform.rotation = Camera.main.transform.rotation;
		buttonRenderer.color = Color.clear;
        lastPos = transform.position;
        animRoot.rotation = Camera.main.transform.rotation;
	}

    // Update is called once per frame
    Vector3 lastPos;
	void Update ()
	{
		HandleDeathTimer ();

		CheckInputs ();

        //for (int i = 0; i < 20; i++)
        //{
        //    if (Input.GetKeyDown("joystick 1 button " + i))
        //    {
        //        print("joystick 1 button " + i);
        //    }
        //}
        //A = Input.GetKeyDown("joystick 1 button 0")
        //B = Input.GetKeyDown("joystick 1 button 1")
        //X = Input.GetKeyDown("joystick 1 button 2")
        //Y = Input.GetKeyDown("joystick 1 button 3")
        //LB = Input.GetKeyDown("joystick 1 button 4")
        //RB = Input.GetKeyDown("joystick 1 button 5")

        Vector3 delta = (transform.position - lastPos);
        delta.z = 0;

        Anim.SetFloat("velocity", delta.magnitude);

        if (delta != Vector3.zero)
        {
            delta.Normalize();

            if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y)){
                //Side
                SetDirection(PlayerDirection.SIDE);
                Vector3 newScale = transform.localScale;
                newScale.x = delta.x > 0 ? -Mathf.Abs(newScale.x) : Mathf.Abs(newScale.x);
                transform.localScale = newScale;
            }
            else {
                if (delta.y > 0){
                    //Back
                    SetDirection(PlayerDirection.BACK);
                }
                else {
                    SetDirection(PlayerDirection.FRONT);
                }
            }
        }

        lastPos = transform.position;
	}

    private void SetDirection(PlayerDirection dir)
    {
        Debug.Log("New direction set: " + dir);
        direction = dir;
        Anim.SetFloat("direction", (float)dir);
    }

	private void HandleDeathTimer ()
	{
		if (!interacting) 
        {
			life -= Time.deltaTime;
			if (life <= 0) 
            {
				life = 0;
			}

			timerImage.fillAmount = life / PLAYER_LIFETIME_S;
			if (state == PlayerState.ALIVE && life <= 0) 
            {
                //Player dies
				PlayerFired ();
			}
		}
	}

	public override void UpdateButtonState (int puzzleButton)
	{
		if (puzzleButton < buttonSprites.Length) {
			buttonRenderer.color = Color.white;
			buttonRenderer.sprite = buttonSprites [puzzleButton];
		}
	}

	private void ClearButtonState ()
	{
		buttonRenderer.color = Color.clear;
		buttonRenderer.sprite = null;
	}

	public override void FailedTask ()
	{
		Debug.Log ("Failed task");
		ClearButtonState ();
		life -= PLAYER_FAILURE_PENALTY_S;
		interacting = false;
	}

	public override void CompletedTask ()
	{
		ClearButtonState ();
		life = PLAYER_LIFETIME_S;
		Debug.Log ("Completed task");
		interacting = false;
	}

	private void PlayerFired ()
	{
		ClearButtonState ();
		state = PlayerState.FIRED;
		Debug.Log ("PLAYER IS FIRED!");
		interacting = false;
        Anim.SetTrigger("accuse_wrong");
	}

	public override void PlayerTrapped ()
	{
		ClearButtonState ();
		state = PlayerState.KILLED;
		Debug.Log ("PLAYER IS KILLED!");
		interacting = false;
        Anim.SetTrigger("die");
	}


	//used to check inputs as fixed can miss input -  set bool flags
	private void CheckInputs ()
	{
		//if current trigger assigned
		if (CurrentTriggerObject && !interacting && state == PlayerState.ALIVE) {
			//if button pushed
            if (data.player.GetButtonDown(Controls.ACTION0)) {
				interacting = CurrentTriggerObject.StartInteraction (this);
			}
		}

		if (interacting) {
			// Check for puzzle buttons
			int result = -3;
            //TODO: Correct the indices, they were decided by different members on the team :)
			if (data.player.GetButtonDown(Controls.ACTION0)) {
				result = CurrentTriggerObject.Interact (this, 0);
			} else if (data.player.GetButtonDown(Controls.ACTION2)) {
				result = CurrentTriggerObject.Interact (this, 1);
			} else if (data.player.GetButtonDown(Controls.ACTION3)) {
				result = CurrentTriggerObject.Interact (this, 2);
			} else if (data.player.GetButtonDown(Controls.ACTION1)) {
				result = CurrentTriggerObject.Interact (this, 3);
			}
//			Debug.Log ("Guess result " + result);

			if (isBetrayer) {
                if (data.player.GetButton(Controls.ACTION0)) {
					if (CurrentTriggerObject.SetTrap (this)) {
						if (currentTrappedObject != null) {
							currentTrappedObject.RemoveTrap (this);
						}
						currentTrappedObject = CurrentTriggerObject;
						Debug.Log ("Set trap");
					} else {
						Debug.Log ("Failed to set trap");
					}
				}

				if (Input.GetKeyUp ("l") && currentTrappedObject) {
					currentTrappedObject.UseTrap (this);
				}
			}

		}
	}

	private void FixedUpdate ()
	{
		// Can only move when not interacting
		if (!interacting)
        {
            Vector3 targetVelocity = Vector3.zero;
            //get axis input
            //targetVelocity = new Vector3(Input.GetAxisRaw("Vertical"), 0, -Input.GetAxisRaw("Horizontal"));
            targetVelocity += new Vector3(data.player.GetAxis(Controls.AXIS_VERT), data.player.GetAxis(Controls.AXIS_HORIZ)) * Speed;

            //if (targetVelocity.sqrMagnitude > maxVelocity * maxVelocity)
            //{
            //    targetVelocity = targetVelocity.normalized * maxVelocity;
            //}

            //RB.AddForce (velocityChange, ForceMode.VelocityChange);
            RB.AddForce(targetVelocity);
            RB.velocity = RB.velocity.magnitude > maxVelocity ? RB.velocity.normalized * maxVelocity : RB.velocity;
		}
	}

	public override void OnInteractionState (InteractableObject obj, bool canInteract)
	{
		if (canInteract && state == PlayerState.ALIVE) {
			CurrentTriggerObject = obj;
			buttonRenderer.color = Color.white;
			buttonRenderer.sprite = buttonSprites [0];
		} else {
			CurrentTriggerObject = null;
			ClearButtonState ();
		}
	}
}
