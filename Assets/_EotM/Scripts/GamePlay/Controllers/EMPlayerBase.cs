﻿using UnityEngine;
using System.Collections;

public abstract class EMPlayerBase : MonoBehaviour
{
    public abstract void UpdateButtonState(int puzzleButton);
    public abstract void PlayerTrapped();
    public abstract void SetPlayerData(PlayerData data);
    public abstract void FailedTask();
    public abstract void OnInteractionState(InteractableObject obj, bool canInteract);
    public abstract void CompletedTask();
    public abstract void SetBetrayer(bool isBetrayer);
}
