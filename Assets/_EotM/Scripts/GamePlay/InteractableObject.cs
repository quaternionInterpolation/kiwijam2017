﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

struct PlayerCooldown
{
	public EMPlayerBase player;
	public float cooldownS;
}

struct Interaction
{
	public EMPlayerBase player;
	public Puzzle puzzle;
}

struct Puzzle
{
	public int current;
	public int[] sequence;
	public float pressTime;
}

public enum TweenState
{
	OPENING,
	CLOSING
}

public enum UIHintState
{
	OPEN,
	CLOSED
}


public class InteractableObject : MonoBehaviour
{
	private const int PUZZLE_LENGTH = 8;
	public const int PUZZLE_BUTTONS = 4;
	public const float PUZZLE_PRESS_TIME_S = 3.0f;

	public const float PLAYER_COOLDOWN_S = 10.0f;

	public InteractableUI InteractUI;
	public bool Opening = false;
	public bool Closing = false;
	public TweenState hintAnimState = TweenState.CLOSING;
	public UIHintState hintState = UIHintState.CLOSED;

	private List<PlayerCooldown> playerCooldowns = new List<PlayerCooldown> ();
	private Interaction currentInteraction;

	private EMPlayerBase trappedByPlayer;

	public virtual void Start ()
	{
		DOTween.Init (true, true, LogBehaviour.Verbose).SetCapacity (100, 10);
//		buttonRenderer = gameObject.GetComponentInChildren<SpriteRenderer> ();
	}

	public void Update ()
	{
		// If any players are triggering the interactable and nobody is interacting, present the interaction UI
		if (collidingPlayers.Count > 0) {
			if (hintAnimState != TweenState.OPENING && hintState == UIHintState.CLOSED) {
//				ResetAll ();
//				InteractUI.gameObject.SetActive (true);
//				StartCoroutine (OpenUI ());
				hintAnimState = TweenState.OPENING;
				hintState = UIHintState.OPEN;
			}
		} else if (hintAnimState != TweenState.CLOSING && hintState == UIHintState.OPEN) {
//			ResetAll ();
//			StartCoroutine (CloseUI ());
//			InteractUI.gameObject.SetActive (false);
			hintAnimState = TweenState.CLOSING;
			hintState = UIHintState.CLOSED;
		}

		UpdatePuzzleTime ();

		UpdateCooldowns ();
	}

	private void UpdatePuzzleTime ()
	{
		if (!currentInteraction.Equals (default(Interaction))) {
			currentInteraction.puzzle.pressTime -= Time.deltaTime;
			if (currentInteraction.puzzle.pressTime <= 0) {
				// The puzzle has been failed
				currentInteraction.player.FailedTask ();
				currentInteraction = default(Interaction);
			}
		}
	}

	private void UpdateCooldowns ()
	{
		for (int ind = 0; ind < playerCooldowns.Count; ind++) {
			PlayerCooldown pCooldown = playerCooldowns [ind];
			if (pCooldown.cooldownS > 0.0f) {
				if (pCooldown.cooldownS - Time.deltaTime <= 0.0f) {
					pCooldown.cooldownS = 0.0f;
				} else {
					pCooldown.cooldownS -= Time.deltaTime;
				}
			}
			playerCooldowns [ind] = pCooldown;
			if (playerCooldowns [ind].cooldownS == 0 && collidingPlayers.Contains (playerCooldowns [ind].player)) {
				if (this.CanInteractWith (playerCooldowns [ind].player)) {
					playerCooldowns [ind].player.OnInteractionState (this, CanInteractWith (playerCooldowns [ind].player));
				}
			}
		}
	}

	private Puzzle GetNewPuzzle ()
	{
		Puzzle puzzle = new Puzzle ();
		puzzle.current = 0;
		puzzle.sequence = new int[PUZZLE_LENGTH];
		for (int i = 0; i < PUZZLE_LENGTH; i++) {
			puzzle.sequence [i] = Random.Range (0, PUZZLE_BUTTONS);
		}
		puzzle.pressTime = PUZZLE_PRESS_TIME_S;
		return puzzle;
	}

	public bool StartInteraction (EMPlayerBase player)
	{
		if (currentInteraction.Equals (default(Interaction))) {
			currentInteraction.player = player;
			currentInteraction.puzzle = GetNewPuzzle ();
			player.UpdateButtonState (currentInteraction.puzzle.sequence [currentInteraction.puzzle.current]);
			return true;
		} else {
			return false;
		}
	}

	public bool IsInteracting (EMPlayerBase player)
	{
		return currentInteraction.player == player;
	}

	public int Interact (EMPlayerBase player, int puzzleGuess)
	{
		if (currentInteraction.player != player) {
			return -2;
		}
		Debug.Log ("Guess " + puzzleGuess + " answer " + currentInteraction.puzzle.sequence [currentInteraction.puzzle.current]);
		if (currentInteraction.puzzle.sequence [currentInteraction.puzzle.current] == puzzleGuess) {
			// Guessed correctly
			if (currentInteraction.puzzle.current++ >= PUZZLE_LENGTH - 1) {
				// Add their cooldown
				SetPlayerCooldown (player, PLAYER_COOLDOWN_S);
				currentInteraction.player.CompletedTask ();
				currentInteraction = default (Interaction);
				// Puzzle complete (can move on)
				return 1;
			} else {
				// Puzzle not yet complete
				currentInteraction.puzzle.pressTime = PUZZLE_PRESS_TIME_S;
				currentInteraction.player.UpdateButtonState (currentInteraction.puzzle.sequence [currentInteraction.puzzle.current]);
				return 0;
			}
		} else {
			currentInteraction.player.FailedTask ();
			currentInteraction = default (Interaction);
			return -1;
		}
	}

	public bool SetTrap (EMPlayerBase player)
	{
		if (!currentInteraction.Equals (default(Interaction)) && currentInteraction.player == player) {
			trappedByPlayer = player;
			return true;
		} else {
			return false;
		}
	}

	public void RemoveTrap (EMPlayerBase player)
	{
		if (trappedByPlayer == player) {
			trappedByPlayer = null;
		}
	}

	public bool UseTrap (EMPlayerBase player)
	{
		if (trappedByPlayer == player) {
			trappedByPlayer = null;
			currentInteraction.player.PlayerTrapped ();
			currentInteraction = default(Interaction);
			return true;
		} else {
			return false;
		}
	}

	private void SetPlayerCooldown (EMPlayerBase player, float cooldownS)
	{
		PlayerCooldown existingCooldown = playerCooldowns.Find (playerCooldown => playerCooldown.player == player);
		if (!existingCooldown.Equals (default(PlayerCooldown))) {
			Debug.Log ("Setting existing cooldown to " + cooldownS);
			existingCooldown.cooldownS = cooldownS;
		} else {
			Debug.Log ("Setting new cooldown to " + cooldownS);
			existingCooldown = new PlayerCooldown ();
			existingCooldown.player = player;
			existingCooldown.cooldownS = cooldownS;
			playerCooldowns.Add (existingCooldown);
		}
	}




	public void InteractWith ()
	{
	}

	private List<EMPlayerBase> collidingPlayers = new List<EMPlayerBase> ();

	public virtual void OnTriggerEnter (Collider other)
	{
		// Add the player to the list of colliding players
		EMPlayerBase playerComponent = other.GetComponent<EMPlayerBase> ();
		if (playerComponent && !collidingPlayers.Contains (playerComponent)) {
			collidingPlayers.Add (playerComponent);
			if (this.CanInteractWith (playerComponent)) {
				playerComponent.OnInteractionState (this, this.CanInteractWith (playerComponent));
			}
		}


//		if (!cooldown) {
//			if (other.CompareTag ("Player")) {
//				//add interactable object to player
//				//set scale to small
//				if (!Opening) {
//					other.GetComponentInParent<EMPlayer> ().SetTrigger (this);
//					ResetAll ();
//					Closing = false;
//					InteractUI.gameObject.SetActive (true);
//					StartCoroutine (OpenUI ());
//				}
//			}
//		}
	}

	public bool CanInteractWith (EMPlayerBase player)
	{
		PlayerCooldown cooldown = playerCooldowns.Find (obj => obj.player == player);
//		Debug.Log ("Player cooldown " + cooldown.cooldownS);
		return currentInteraction.Equals (default(Interaction)) && (cooldown.Equals (default(PlayerCooldown)) || cooldown.cooldownS <= 0);
	}

	private void OnTriggerExit (Collider other)
	{
		// Remove the player from the list of colliding players
		EMPlayer playerComponent = other.GetComponent<EMPlayer> ();

		if (playerComponent && collidingPlayers.Contains (playerComponent)) {
			collidingPlayers.Remove (playerComponent);
			playerComponent.OnInteractionState (this, false);
		}

		// If the currently interacting player is leaving, reset the current interaction
		if (currentInteraction.player == playerComponent) {
			currentInteraction = default(Interaction);
		}


		// If no players are currently triggering the interactable then hide the interaction UI again
		if (currentInteraction.Equals (default(Interaction))) {
			currentInteraction = default(Interaction);
		}
		//		if (!cooldown) {
		//			if (other.CompareTag ("Player")) {
		//				//close UI
		//				if (!Closing) {
		//					Opening = false;
		//					ResetAll ();
		//					StartCoroutine (CloseUI ());
		//					//InteractUI.gameObject.SetActive(false);
		//				}
		//			}
		//		}
	}

	//	public virtual IEnumerator CloseUI ()
	//	{
	//		hintAnimState = TweenState.CLOSING;
	//		yield return new WaitForEndOfFrame ();
	//		InteractUI.ButtonSprite.transform.DOScale (Vector3.one / 10, 0.15f);
	//		yield return new WaitForSeconds (0.1f);
	//		InteractUI.Background.transform.DOScale (Vector3.one / 10, 0.15f);
	//		hintState = UIHintState.CLOSED;
	//	}
	//
	//	private void ResetAll ()
	//	{
	//		DOTween.Kill (InteractUI.Background);
	//		DOTween.Kill (InteractUI.ButtonSprite);
	//		StopAllCoroutines ();
	//	}
	//
	//	public virtual IEnumerator OpenUI ()
	//	{
	//		hintAnimState = TweenState.OPENING;
	//		DOTween.Kill (InteractUI);
	//		//background sprite fun
	//		//reset scale
	//		InteractUI.Background.transform.localScale = Vector3.one;
	//		//set xscale to be small
	//		Vector3 tempScale = InteractUI.Background.transform.localScale;
	//		tempScale.x = 0.1f;
	//		InteractUI.Background.transform.localScale = tempScale;
	//		//store color for alpha scale
	//		Color TempColor = InteractUI.Background.color;
	//		TempColor.a = 0.01f;
	//		InteractUI.Background.color = TempColor;
	//		//store pos
	//		Vector3 Position = InteractUI.BGStartpos;
	//		//Background.transform.position;
	//		//XSprite stuff
	//		//maybe rotate it?
	//		//reset scale
	//		InteractUI.ButtonSprite.transform.localScale = Vector3.one;
	//		Color SpriteColor = InteractUI.ButtonSprite.color;
	//		SpriteColor.a = 0.5f;
	//		InteractUI.ButtonSprite.color = SpriteColor;
	//		while (InteractUI.Background.color.a < 0.6f) {
	//			TempColor.a += 0.08f;
	//			InteractUI.Background.color = TempColor;
	//			SpriteColor.a += 0.08f;
	//			InteractUI.ButtonSprite.color = SpriteColor;
	//			//12 iterations
	//			Position.y += 1;
	//			InteractUI.ButtonSprite.transform.position = Position;
	//			InteractUI.Background.transform.position = Position;
	//			yield return new WaitForEndOfFrame ();
	//		}
	//		InteractUI.Background.transform.DOScale (Vector3.one, 0.3f);
	//		Opening = false;
	//		yield return new WaitForSeconds (0.1f);
	//		hintState = UIHintState.OPEN;
	//		InteractUI.ButtonSprite.transform.localScale = Vector3.one;
	//	}

}
