﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InteractableUI : MonoBehaviour
{

    public Image Background;
    public Image ButtonSprite;
    public GameObject Timer;
    public Material TimerMat;
    public Vector3 BGStartpos;

    private void Start()
    {
        BGStartpos = Background.transform.position;
        Timer.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
    public void SetTimerFill(float Percent)
    {
        if (!Timer.gameObject.activeInHierarchy)
        {
            Timer.gameObject.SetActive(true);
        }
        TimerMat.SetFloat("_ColorRampOffset", Percent);
        //Timer.fillAmount =Percent;
    }
}
