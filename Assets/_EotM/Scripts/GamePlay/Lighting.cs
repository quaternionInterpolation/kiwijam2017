﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighting : MonoBehaviour
{

    public bool night = false;
    public Light mainLight;
    private float amount=0;
    public float increment;
   

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    { 
        if (night)
            StartNight();
    }

    public void StartNight()
    {
        night = false;
        StartCoroutine(LerpLight());
        Debug.Log("night tick");
    }

    private IEnumerator LerpLight()
    {
        while (amount <0.7)
        {
            amount += increment;
            yield return new WaitForSeconds(0.001f);
            mainLight.color = Color.Lerp(Color.white, Color.black, amount);
        }
    }
}
