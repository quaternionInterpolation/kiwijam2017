﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using DG.Tweening;
public class AccuseScreen : MonoBehaviour {

    public List<PlayerAccuseInfo> PlayerList = new List<PlayerAccuseInfo>();
    private bool moving = false;
	// Use this for initialization
	void Start () {
        foreach (var item in PlayerList)
        {
            item.gameObject.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (!moving)
                StartCoroutine(Transition());
        }
        
	}
    private IEnumerator Transition()
    {
        moving = true;
       
        foreach (var item in PlayerList)
        {
            item.gameObject.SetActive(true);
            foreach (Transform child in item.transform)
            {
                child.localScale = Vector3.zero;
                Vector3 temp = child.position;
                child.position = Vector3.zero;
                child.DOMove(temp, Random.Range(0.1f, 0.2f));
                child.DOScale(Vector3.one, Random.Range(0.1f, 0.2f));
            }
        }
        yield return new WaitForSeconds(0.2f);
        moving = false;
    }
}
