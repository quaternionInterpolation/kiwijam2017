using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;
using UnityEngine.SceneManagement;

public class GameManager : SingletonMonobehaviour<GameManager>
{
    public string levelName = "Level";

    private string curSceneLoaded;
    public bool Faded=false;
    //TODO: Fade screen
    //public Image screenFade

    protected override void Awake()
    {
		base.Awake();
		Timing.CallDelayed(0.3f, ControllerHelper.Init);
	}

    public void LoadLevel(string levelName){
        Timing.RunCoroutine(LoadLevelCoroutine(levelName));
    }

	private IEnumerator<float> LoadLevelCoroutine(string levelName)
	{
        //Fade out

        //Unload last scene
        if (curSceneLoaded != null){
            AsyncOperation unloadOp = SceneManager.UnloadSceneAsync(curSceneLoaded);
            yield return Timing.WaitUntilDone(unloadOp);
        }

        if (levelName != null){
			AsyncOperation loadOp = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
			yield return Timing.WaitUntilDone(loadOp);
			yield return Timing.WaitForOneFrame;
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(levelName));
        }

        //Fade in

        curSceneLoaded = levelName;
        Debug.Log("Loaded scene "+levelName);
	}
}
