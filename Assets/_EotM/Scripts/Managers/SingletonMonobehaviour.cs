﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonMonobehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T sInstance = null;
	public static T Instance
	{
		get
		{
			if (sInstance == null)
			{
				sInstance = FindObjectOfType<T>();
			}

			return sInstance;
		}
	}

	// Use this for initialization
	protected virtual void Awake()
	{
		sInstance = GetComponent<T>();
	}
}
