﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

	public bool used { get; private set; }

	public void UseSpawnPoint() {
		used = true;
	}

	public void ResetSpawnPoint() {
		used = false;
	}

    public EMPlayerBase Spawn(Transform prefab)
    {
        Transform instance = Instantiate(prefab, transform.position, Quaternion.identity);
        return instance.GetComponent<EMPlayerBase>();
    }
}
