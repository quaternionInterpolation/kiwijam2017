using UnityEngine;
using System.Collections;

public class PlayerData
{
    public int playerID;
    public Rewired.Player player;
    public Rewired.Controller controller;
    public EMPlayerBase gamePlayer;

    public PlayerData(){
        //Stub
    }

    public PlayerData(int playerID, Rewired.Player player, Rewired.Controller controller, EMPlayerBase gamePlayer){
        this.playerID = playerID;
        this.player = player;
        this.controller = controller;
        this.gamePlayer = gamePlayer;
    }
}
