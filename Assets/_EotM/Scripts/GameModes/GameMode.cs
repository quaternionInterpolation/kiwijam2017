using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using System;

public class GameMode : MonoBehaviour
{
	protected List<PlayerData> players = new List<PlayerData>();

    protected virtual void Awake()
    {
        DebugHelper.PerformDebugLaunchCheck();
    }

	protected virtual void Start()
    {
		//Stub
	}

	public void RemovePlayerData(PlayerData player)
	{
		players.Remove(player);
	}

	public void RemovePlayerData(int playerIndex)
	{
		RemovePlayerData(players.Find((obj) => obj.playerID == playerIndex));
	}

	public void SetPlayerData(int playerIndex, PlayerData data)
	{
		int existingIndex = players.FindIndex((obj) => obj.playerID == playerIndex);

		if (existingIndex != -1)
		{
			players[existingIndex] = data;
		}
		else
		{
			players.Add(data);
		}
	}

    public virtual PlayerData GetPlayerFromController(Controller curController)
    {
        //TODO: Not sure if this requires controller specific IDs
        return players.Find((obj) => obj.controller == curController);
    }

    public List<PlayerData> GetPlayers(){
        return players;
    }
}
