using UnityEngine;
using System.Collections;

public class BasicGameMode : GameMode
{
    public enum State
    {
        PlayerSelect,
        Setup,
        MainGame
    }

    public PlayerSelectScreen playerSelectScreen;
    //public 

    [HideInInspector]
    public State curState;
    //Interactables

    //Spawn points
    private SpawnPoint[] spawnPoints;
    //private EMPlayer[] playersTesting; //TODO: Delete this. Use GameMode players list. Used for testing
    public Transform playerPrefab;

    protected override void Awake()
    {
        base.Awake();

        SetState(State.PlayerSelect);
        playerSelectScreen.SetGameMode(this);
    }

    protected override void Start() {
        base.Start();
		//Spawn points

		//TODO: This is a horrible assumption that we will have >= 4 spawn points
		spawnPoints = FindObjectsOfType(typeof(SpawnPoint)) as SpawnPoint[];
    }

	public void SetState(State state)
	{
		playerSelectScreen.gameObject.SetActive(state == State.PlayerSelect);

        switch (state){
            case State.Setup:
                //Spawn players
                SpawnPlayers();
                break;

            case State.MainGame:
                //Allow people to be murdered and traps to be triggered at this point
                break;
        }
			
        curState = state;
    }

    public void SpawnPlayers()
    {
        foreach(PlayerData player in players) {
            SpawnPoint foundSpawnPoint = null;

            // Pick a random spawn point that hasn't been used
            while(foundSpawnPoint == null) {
                SpawnPoint sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
                if(!sp.used) {
                    foundSpawnPoint = sp;
                }
            }
            foundSpawnPoint.UseSpawnPoint(); // Say that we used the spawn point so it won't be used for the next player

            EMPlayerBase playerScript = foundSpawnPoint.Spawn(playerPrefab);

			//Add this player
			player.gamePlayer = playerScript;
			playerScript.SetPlayerData(player); //This is what 2 nights out drinking and 4 hours sleep looks like in code form.
		}

		//Once we are done spawn the players, reset all the SpawnPoints so they can be used in the next round.
		for (int i = 0; i < spawnPoints.Length; ++i)
		{
            spawnPoints[i].ResetSpawnPoint();
        }

        TopDownCameraXBuild tdc = FindObjectOfType<TopDownCameraXBuild>();
		foreach (PlayerData player in players)
		{
			tdc.AddFocusPoint(player.gamePlayer.gameObject);
		}
		// TODO: Setup camera after players spawn because it needs to find all the players in the scene.
		Camera.main.GetComponent<TopDownCameraXBuild>().SetupCamera();
    }
}
