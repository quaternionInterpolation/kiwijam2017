﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : SingletonMonobehaviour<SoundManager> {

    public AudioClip MainTrack;
    private AudioSource Source;
	// Use this for initialization
	void Start ()
    {
        Source = GetComponent<AudioSource>();
        Source.loop = true;
        Source.clip = MainTrack;
        Source.Play();
    }
	
	// Update is called once per frame
	void Update ()
    {
        
    }
}
