using UnityEngine;
using System.Collections;
using Rewired;
using System.Collections.Generic;

public class ControllerHelper
{
    public static bool AnyUpPressed()
    {
        return AnyControllerPressedButton(Controls.MOVE_UP);
	}

	public static bool AnyDownPressed()
	{
		return AnyControllerPressedButton(Controls.MOVE_DOWN);
	}

	public static bool AnyPositivePressed()
	{
		return AnyControllerPressedButton(Controls.ACTION0);
	}

	public static bool AnyNegativePressed()
	{
		return AnyControllerPressedButton(Controls.ACTION1);
	}

    public static bool AnyControllerPressedButton(string actionString)
    {
        Player systemPlayer = Rewired.ReInput.players.SystemPlayer;
        return systemPlayer.GetButtonDown(actionString);
    }

    public static void Init(){
		Player systemPlayer = Rewired.ReInput.players.SystemPlayer;
        foreach (Controller controller in ReInput.controllers.Controllers){
            systemPlayer.controllers.AddController(controller, false);
		}

        foreach (Controller controller in systemPlayer.controllers.Controllers){
            Debug.Log("SYSTEM CONTROLLER: " + controller.name);
        }
    }
}
