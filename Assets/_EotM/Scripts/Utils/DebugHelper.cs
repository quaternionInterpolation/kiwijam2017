﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DebugHelper
{
    private static bool debugInit = false;

    public static void PerformDebugLaunchCheck()
    {
#if UNITY_EDITOR
        if (!debugInit && Application.isPlaying && Application.isEditor)
        {
            debugInit = true;
            SceneManager.LoadScene(0);
        }
#endif  
    }
}
