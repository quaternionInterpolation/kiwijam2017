using UnityEngine;
using System.Collections;

public class Controls
{
	public static readonly string MOVE_UP = "Menu Up";
	public static readonly string MOVE_DOWN = "Menu Down";
	public static readonly string AXIS_VERT = "Move Horizontal";
	public static readonly string AXIS_HORIZ = "Move Vertical";
    public static readonly string ACTION0 = "Action0";  //A on xbox
    public static readonly string ACTION1 = "Action1";  //B on xbox
    public static readonly string ACTION2 = "Action2";  //X on xbox
    public static readonly string ACTION3 = "Action3";  //Y on xbox
    public static readonly string ACCUSE = "Accuse";
    public static readonly string MENU = "Menu";
}
