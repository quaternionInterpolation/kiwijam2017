using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using MovementEffects;
using System.Collections.Generic;

public class LaunchScript : MonoBehaviour
{
    public string menuScene = "Menu";

	// Use this for initialization
	void Awake()
	{
        //Launch the menu
        GameManager.Instance.LoadLevel(menuScene);
	}
}
