﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class ControllerTester : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < ReInput.players.playerCount; ++i)
        {
			Rewired.Player p = ReInput.players.GetPlayer(i);

            if (p.controllers.joystickCount == 0){
				foreach (Controller controller in ReInput.controllers.Controllers){
					if (controller.type != ControllerType.Joystick) break;
					
					if (controller.GetAnyButtonDown()){
						//This is the current input
						if (!p.controllers.ContainsController(controller)){
							//p.controllers.ClearAllControllers();
							p.controllers.AddController(controller, true);
						}
					}
				}            
            }

            if (p.controllers.joystickCount > 0){
				TestButton(p, Controls.ACTION0);
				TestButton(p, Controls.ACTION1);
				TestButton(p, Controls.ACTION2);
				TestButton(p, Controls.ACTION3);
				TestButton(p, Controls.MENU);
				TestButton(p, Controls.ACCUSE);            
            }
        }
	}

    private void TestButton(Rewired.Player p, string button){
        if (p.GetButtonDown(button)){
            Debug.Log("PLAYER "+p.id+" BUTTON PRESS: "+button);
        }
    }
}
