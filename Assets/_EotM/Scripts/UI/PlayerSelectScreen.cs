using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerSelectScreen : MonoBehaviour
{
    private BasicGameMode gameMode;

    public int REQUIRED_PLAYERS = 4;

    private bool startingGame = true;

    public PlayerSelectCell[] cells;
    public Text continueText;

    private void Awake()
	{
		for (int i = 0; i < REQUIRED_PLAYERS; ++i)
		{
			cells[i].SetShowingCell(false);
		}

		continueText.DOFade(0f, 0f).Complete();
    }

    public void SetGameMode(BasicGameMode gameMode){
		this.gameMode = gameMode;
    }

    //Some UI shit
    public void Update()
    {
        if (!startingGame) return;

        //Poll for input from controllers
        foreach (Controller curController in ReInput.controllers.Controllers)
        {
            if (curController.type == ControllerType.Mouse) continue;

            PlayerData existingPlayer = gameMode.GetPlayerFromController(curController);

            if (existingPlayer != null)
            {
                Rewired.Player p = existingPlayer.player;

                // Back button
                if (p.GetButtonDown(Controls.ACTION1)) 
                {
                    //Disable the controller
                    gameMode.RemovePlayerData(existingPlayer);
					UpdateUI();
                }

                if (p.GetButtonDown(Controls.MENU))
				{
                    //Begin sequence
                    if (gameMode.GetPlayers().Count >= REQUIRED_PLAYERS){
						startingGame = false;
						OnAllPlayersReady();                  
                    }
                    else {
                        Debug.Log("TODO: But you require more players to begin the game");
                    }
                }
            }
            else
            {
                //We don't have a player for this one yet, let's add them if they press a button
                if (curController.GetAnyButtonDown())
                {
                    int newPlayerId = gameMode.GetPlayers().Count;
                    Player newPlayerPlayer = ReInput.players.GetPlayer(newPlayerId);
                    newPlayerPlayer.controllers.ClearAllControllers();
                    newPlayerPlayer.controllers.AddController(curController, false);
                    PlayerData p = new PlayerData(newPlayerId, newPlayerPlayer, curController, null);
                    gameMode.SetPlayerData(newPlayerId, p);               
                    UpdateUI();
                }
            }
        }
    }

    public void UpdateUI(){
        //Update all the UI
        List<PlayerData> players = gameMode.GetPlayers();
        for (int i = 0; i < 4; ++i)
        {
            PlayerData curPlayer = null;
            if (i < players.Count)
            {
                curPlayer = players[i];
                cells[i].SetPlayer(i, curPlayer.controller.name);
            }

            cells[i].SetShowingCell(curPlayer != null);
        }

        continueText.DOFade(players.Count >= REQUIRED_PLAYERS ? 1f : 0f, 0.25f);
    }

    private void OnAllPlayersReady(){
        gameMode.SetState(BasicGameMode.State.Setup);
    }
}
