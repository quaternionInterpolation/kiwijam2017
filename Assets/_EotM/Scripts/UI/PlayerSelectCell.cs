using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using DG.Tweening;

public class PlayerSelectCell : MonoBehaviour
{
    public Text text;
    public Text controller;
    public Transform root;

    private void Awake()
    {
        root.DOScale(0f, 0f).Complete();
    }

    public void SetPlayer(int player, string controller){
        text.text = "PLAYER " + player;
        this.controller.text = controller;
    }

    public void SetShowingCell(bool showing){
        root.DOScale(showing ? 1f : 0f, 0.25f);
    }
}
