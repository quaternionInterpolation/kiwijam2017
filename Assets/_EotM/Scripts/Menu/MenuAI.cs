﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MenuAI : MonoBehaviour
{
    private NavMeshAgent Agent;
    public float WalkRadi;
    private MenuAIModel MyModel;
    private float IdleTime = 5;
    Vector3 NewTarget= Vector3.zero;
	// Use this for initialization
	void Start ()
    {
        MyModel = GetComponentInChildren<MenuAIModel>();
        Agent = GetComponent<NavMeshAgent>();
        GetNewPoint();

    }
	
	// Update is called once per frame
	void Update () {
        if (!Agent.hasPath)
        {
            if (IdleTime < 0)
            {
                
                if (GetNewPoint())
                {
                    IdleTime = Random.Range(1.5f, 3.5f);
                    Agent.SetDestination(NewTarget);

                    MyModel.Walking();
                }
            }
            else
            {
                MyModel.Idle();
                IdleTime -= Time.deltaTime;
            }
        }
	}
    private bool GetNewPoint()
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomDirection = transform.position + Random.insideUnitSphere * WalkRadi;

            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomDirection, out hit, WalkRadi, 1))
            {
                NewTarget = hit.position;
                return true;
            } 
          
        }
      



        return false;
    }
}
