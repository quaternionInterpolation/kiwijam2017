using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using MovementEffects;

public class MenuScript : MonoBehaviour
{
    public MenuButton SelectedButton;
    public MenuButton[] ButtonArray;
    private int MenuIndex =0;

    private void Awake()
    {
        DebugHelper.PerformDebugLaunchCheck();
    }

    private void Start()
    {
        SelectButton(ButtonArray[0]);
    }

    private void SelectButton(MenuButton Select)
    {
        if (SelectedButton)
        {
            SelectedButton.gameObject.transform.DOScale(Vector3.one, 0.15f);
        }
        SelectedButton = Select;

        SelectedButton.gameObject.transform.DOScale(Vector3.one*1.3f,0.15f);
    }

    private void Update()
    {
        if (ControllerHelper.AnyUpPressed())
        {
            MenuIndex += 1;
            if (MenuIndex == ButtonArray.Length)
            {
                MenuIndex = 0;
            }
            SelectButton(ButtonArray[MenuIndex]);
        }

		if (ControllerHelper.AnyDownPressed())
        {
            MenuIndex -= 1;
            if (MenuIndex == -1)
            {
                MenuIndex = ButtonArray.Length-1;
            }
            SelectButton(ButtonArray[MenuIndex]);
        }

        if (ControllerHelper.AnyPositivePressed())
        {
            SelectedButton.Activate();
        }

    }
}
