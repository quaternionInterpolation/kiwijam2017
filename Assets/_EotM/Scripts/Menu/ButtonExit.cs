﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonExit : MenuButton {

    public override void Activate()
    {
        Application.Quit();
    }
}
