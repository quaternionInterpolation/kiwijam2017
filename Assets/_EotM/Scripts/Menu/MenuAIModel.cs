﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
public class MenuAIModel : MonoBehaviour
{
    [Header("Graphics")]
    public SkeletonAnimation skeletonAnimation;

    [Header("Animation")]
    [SpineAnimation(dataField: "skeletonAnimation")]
    public string walkName = "Walk";
    [SpineAnimation(dataField: "skeletonAnimation")]
    public string runName = "Run";
    [SpineAnimation(dataField: "skeletonAnimation")]
    public string idleName = "Idle";
    // Use this for initialization
    void Start()
    {
        skeletonAnimation.AnimationName = idleName;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool Walking()
    {
        if (skeletonAnimation.AnimationName == walkName)
            return true;
        else
            skeletonAnimation.AnimationName = walkName;
        return true;

    }
    public bool Idle()
    {
        if (skeletonAnimation.AnimationName == idleName)
            return true;
        else
            skeletonAnimation.AnimationName = idleName;
        return true;

    }
}
